// huzzah32-fhelper-mqtt
// Author : TheFwGuy
// Version : 1.1
// Date : April 2019
// 
// starting code from : Rui Santos - https://randomnerdtutorials.com  
//
// The IoT MQTT client described here is reading a soil humidity sensor, sending a reading every 25 seconds to an IoT server
// This client has three MQTT output topics :
//   esp32/temperature    --> ambient temperature
//   esp32/amhumidity     --> ambient humidity
//   esp32/humidity       --> soil humidity
// This client has one MQTT input topics :
//   esp32/output         --> LED control
//
// This sketch need to have installed these libraries :
//  - DHT sensor library from Adafruit
//  - Adafruit Unified Sensor from Adafruit
//  - WiFi library
//  - 


#include <Adafruit_Sensor.h>
#include <DHT.h>

#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>

//#include <Adafruit_BME280.h>


#define DHTPIN  14       // Pin connected to the DHT sensor
#define DHTTYPE DHT22   // Type of sensor

DHT dht(DHTPIN, DHTTYPE);

// SSID/Password combination
const char* ssid = "ElyxorArgentaM";
const char* password = "Pyramid2019Elyxor";
//const char* ssid = "stevear";
//const char* password = "e9e48db9f3";

// MQTT Broker IP address, example:
const char* mqtt_server = "192.168.27.10";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
int reconnectWD = 0;

float humidity = 0;

// LED Pin
const int alarmLED = 12;
const int connectLED = 13;

void(* resetFunc) (void) = 0;

void setup() 
{
  Serial.begin(115200);

  pinMode(alarmLED, OUTPUT);
  pinMode(connectLED, OUTPUT);

  digitalWrite(alarmLED, LOW);
  digitalWrite(connectLED, LOW);

  dht.begin();

  setup_wifi();

  digitalWrite(connectLED, HIGH);

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

void setup_wifi() 
{
  int wifi_watchdog = 0;
  
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.disconnect(true);
  delay(1000);
  WiFi.enableSTA(true);
  delay(1000);  
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) 
  {
    if (wifi_watchdog++ > 50)
    {
      Serial.println("WiFi takes too long - restart !");
      resetFunc();
    }
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length) 
{
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) 
  {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Feel free to add more if statements to control more GPIOs with MQTT

  // If a message is received on the topic esp32/output, you check if the message is either "on" or "off". 
  // Changes the output state according to the message
  if (String(topic) == "esp32/output") 
  {
    Serial.print("Changing output to ");
    if(messageTemp == "on"){
      Serial.println("on");
      digitalWrite(alarmLED, HIGH);
    }
    else if(messageTemp == "off")
    {
      Serial.println("off");
      digitalWrite(alarmLED, LOW);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    reconnectWD++;
    // Attempt to connect
    if (client.connect("ESP8266Client")) 
    {
      Serial.println("connected");
      // Subscribe
      client.subscribe("esp32/output");
      reconnectWD = 0;
      digitalWrite(connectLED, HIGH);
    }
    else
    {
      if (reconnectWD == 30)
      {
        Serial.println("MQTT not connect - restart !");
        resetFunc();    // Restart everything
      }
      digitalWrite(connectLED, LOW);
      
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
      digitalWrite(connectLED, HIGH);
    }
  }
}

void loop() 
{
  int index;
  float amTemp;
  float amHum;

  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();

  // Send out reading every 25 seconds
  long now = millis();
  if (now - lastMsg > 25000) 
  {
    lastMsg = now;

    // Read soil humidity sensor 10 times - with 100 ms delay
    humidity = 0;
    for (index=0; index < 10; index++)
    {
      humidity += analogRead(A2);      //Read sensor on Analog 2;
      delay(100);
    }
    // Calculate average
    humidity /= 10;
        
    // Convert the value to a char array and publish on topic
    char convString[10];
    dtostrf(humidity, 1, 2, convString);
    Serial.print("Humidity: ");
    Serial.println(convString);
    client.publish("esp32/humidity", convString);

    // Reading temperature and ambient humidity
    // Temperature in Celsius
    amTemp = dht.readTemperature();
    if (!isnan(amTemp)) 
    {
       // Convert the value to a char array and publish on topic
       dtostrf(amTemp, 1, 2, convString);
       Serial.print("Temperature : ");
       Serial.println(convString);

       client.publish("esp32/temperature", convString);
    }
  
    // Get humidity event and print its value.
    amHum = dht.readHumidity();
    if (!isnan(amHum)) 
    {
       // Convert the value to a char array and publish on topic
       dtostrf(amHum, 1, 2, convString);
       Serial.print("Ambient Humidty : ");
       Serial.println(convString);
    
       client.publish("esp32/amhumidity", convString);
    }
  }
}
